from typing import Union

import pandas as pd
import pytorch_lightning as pl
from sklearn import preprocessing
from torch.utils.data import DataLoader
from transformers import AutoTokenizer

from .EmotionDataset import EmotionDataset


class EmotionDataModule(pl.LightningDataModule):
    def __init__(self,
                 train_path: str,
                 test_path: str,
                 val_path: str,
                 batch_size: int,
                 max_length: int,
                 num_workers: int,
                 base_model: str):
        """
        EmotionDataModule class for loading and processing emotion
        classification datasets.

        Args:
            train_path (str): Path to the training dataset file.
            test_path (str): Path to the testing dataset file.
            val_path (str): Path to the validation dataset file.
            batch_size (int): Batch size for data loaders.
            max_length (int): Maximum sequence length for tokenization.
            num_workers (int): Number of workers for DataLoader.
            base_model (str): Name or path of the pre-trained transformer model.
        """
        super().__init__()
        self.train_path = train_path
        self.test_path = test_path
        self.val_path = val_path
        self.batch_size = batch_size
        self.max_length = max_length
        self.num_workers = num_workers
        self.base_model = base_model
        self.tokenizer = AutoTokenizer.from_pretrained(self.base_model)
        self.le = preprocessing.LabelEncoder()

    def setup(self, stage: Union[str, None] = None):
        """
        Setup method to initialize datasets.
        LabelEncoder using for labels.

        Args:
            stage (str, optional): Stage of the LightningModule. Defaults to None.
        """
        if stage == 'train' or stage is None:
            train_data = pd.read_csv(self.train_path)
            val_data = pd.read_csv(self.val_path)

            train_texts = train_data['text'].tolist()
            train_labels = train_data['labels'].tolist()
            train_source = train_data['source'].tolist()

            val_texts = val_data['text'].tolist()
            val_labels = val_data['labels'].tolist()
            val_source = val_data['source'].tolist()


            train_labels = self.le.fit_transform(train_labels)
            val_labels = self.le.transform(val_labels)

            self.train_dataset = EmotionDataset(train_texts,
                                                train_labels,
                                                train_source,
                                                self.tokenizer,
                                                self.max_length)

            self.val_dataset = EmotionDataset(val_texts,
                                              val_labels,
                                              val_source,
                                              self.tokenizer,
                                              self.max_length)

        if stage == 'test' or stage is None:
            test_data = pd.read_csv(self.test_path)

            test_texts = test_data['text'].tolist()
            test_labels = test_data['labels'].tolist()
            test_source = test_data['source'].tolist()

            if stage == 'test':
                test_labels = self.le.fit_transform(test_labels)
            else:
                test_labels = self.le.transform(test_labels)

            self.test_dataset = EmotionDataset(test_texts,
                                               test_labels,
                                               test_source,
                                               self.tokenizer,
                                               self.max_length)

    def train_dataloader(self) -> DataLoader:
        """
        Training data loader.

        Returns:
            DataLoader: Training data loader.
        """
        return DataLoader(self.train_dataset,
                          batch_size=self.batch_size,
                          num_workers=self.num_workers)

    def test_dataloader(self) -> DataLoader:
        """
        Testing data loader.

        Returns:
            DataLoader: Testing data loader.
        """
        return DataLoader(self.test_dataset,
                          batch_size=self.batch_size,
                          num_workers=self.num_workers)

    def val_dataloader(self) -> DataLoader:
        """
        Validation data loader.

        Returns:
            DataLoader: Validation data loader.
        """
        return DataLoader(self.val_dataset,
                          batch_size=self.batch_size,
                          num_workers=self.num_workers)

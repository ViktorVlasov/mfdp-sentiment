import torch
from torch.utils.data import Dataset
from transformers import BatchEncoding, PreTrainedTokenizer


class EmotionDataset(Dataset):
    def __init__(
        self,
        texts: list,
        labels: list,
        source: list,
        tokenizer: PreTrainedTokenizer,
        max_length: int
    ):
        """
        Emotion dataset for text classification.

        Args:
            texts (list): List of input texts.
            labels (list): List of corresponding labels.
            tokenizer (PreTrainedTokenizer): Pretrained tokenizer for tokenization.
            max_length (int): Maximum sequence length for tokenization.
        """
        self.texts = texts
        self.labels = labels
        self.source = source
        self.tokenizer = tokenizer
        self.max_length = max_length

    def __len__(self) -> int:
        """
        Return the total number of samples in the dataset.
        """
        return len(self.texts)

    def __getitem__(self, idx: int) -> dict:
        """
        Return the data sample at the given index.

        Args:
            idx (int): Index of the sample.

        Returns:
            dict: Dictionary containing input ids, attention mask, label and source.
        """
        text = str(self.texts[idx])
        label = self.labels[idx]
        source = self.source[idx]

        encoding: BatchEncoding = self.tokenizer.encode_plus(
            text,
            add_special_tokens=True,
            max_length=self.max_length,
            padding='max_length',
            truncation=True,
            return_tensors='pt'
        )

        input_ids = encoding['input_ids'].squeeze()
        attention_mask = encoding['attention_mask'].squeeze()

        return {
            'input_ids': input_ids,
            'attention_mask': attention_mask,
            'label': torch.tensor(label),
            'source': source
        }

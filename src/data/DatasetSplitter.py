from typing import Tuple

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


class DatasetSplitter:
    def __init__(self,
                 target_col: str,
                 source_col: str = 'source',
                 test_size: float = 0.2,
                 val_size: float = 0.2,
                 random_state: int = 42):
        """
        Initialize a DataSplitter instance.

        Parameters:
            target_col (str): The column name containing the target variable.
            source_col (str): The column name containing the data source information.
            test_size (float): The proportion of data to be included in the test set.
            val_size (float): The proportion of data to be included in the validation
            set.
            random_state (int): The random seed for reproducibility.
        """
        self.target_col = target_col
        self.source_col = source_col
        self.test_size = test_size
        self.val_size = val_size
        self.random_state = random_state


    def split_dataset_by_emotion_and_source(
        self,
        data: pd.DataFrame,
    ) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
        """
        Splits the dataset into training, validation, and testing sets considering
        emotion labels and data sources.

        Args:
            data (pd.DataFrame): The original dataset containing sentences and emotion
            labels.
            random_state (int): The random seed for reproducibility.

        Returns:
            A tuple of three DataFrames: (train_data, val_data, test_data)
            train_data: The training data.
            val_data: The validation data.
            test_data: The testing data.
        """
        train_data = pd.DataFrame(columns=[self.source_col, self.target_col])
        val_data = pd.DataFrame(columns=[self.source_col, self.target_col])
        test_data = pd.DataFrame(columns=[self.source_col, self.target_col])

        sources = data[self.source_col].unique()

        for source in sources:
            source_data = data[data[self.source_col] == source]
            labels = source_data[self.target_col].unique()

            for label in labels:
                label_data = source_data[source_data[self.target_col] == label]

                train, test_val = train_test_split(
                    label_data,
                    test_size=self.test_size + self.val_size,
                    random_state=self.random_state
                )
                test, val = train_test_split(
                    test_val,
                    test_size=self.val_size / (self.test_size+self.val_size),
                    random_state=self.random_state
                )

                train_data = pd.concat([train_data, train])
                val_data = pd.concat([val_data, val])
                test_data = pd.concat([test_data, test])

        train_data = train_data.sample(frac=1, random_state=self.random_state)
        train_data = train_data.reset_index(drop=True)

        return train_data, val_data, test_data

    def split_dataset(self,
                      df: pd.DataFrame,
                      balance_classes: bool = True
                      ) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
        """
        Split the dataset into train, validation, and test sets.

        Parameters:
            df (pd.DataFrame): The input DataFrame.
            balance_classes (bool): Whether to balance the classes in the dataset.

        Returns:
            Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]: The train, validation,
            and test DataFrames.
        """
        if balance_classes:
            df = self.balance_classes(df)

        num_samples = len(df)
        test_size = int(self.test_size * num_samples)
        val_size = int(self.val_size * num_samples)
        train_size = num_samples - (test_size + val_size)

        train_df, val_df, test_df = np.split(
            df.sample(frac=1, random_state=self.random_state),
            [train_size, train_size + val_size]
        )

        return pd.DataFrame(train_df), pd.DataFrame(val_df), pd.DataFrame(test_df)


    def balance_classes(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        Balance the classes in the given DataFrame.

        Parameters:
            df (pd.DataFrame): The DataFrame containing the data to balance.

        Returns:
            pd.DataFrame: The balanced DataFrame.
        """
        class_counts = df[self.target_col].value_counts()
        min_class_count = class_counts.min()
        balanced_df = pd.concat(
            [df[df[self.target_col] == cls].sample(min_class_count,
                                                   random_state=self.random_state)
             for cls in class_counts.index]
        )

        return balanced_df

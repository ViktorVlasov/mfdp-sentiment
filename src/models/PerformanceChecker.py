import time

import mlflow
import pandas as pd
import torch
from transformers import AutoTokenizer


class PerformanceChecker():
    def __init__(self, model, cfg, path_to_data) -> None:
        self.model = model
        self.cfg = cfg
        self.path_to_data = path_to_data
        self.tokenizer = AutoTokenizer.from_pretrained(self.cfg.LEARNING.BASE_MODEL)

    def start_check(self) -> None:
        df = pd.read_csv(self.path_to_data)
        self.texts = df['text'].tolist()
        perfomance_times = dict()

        cpu_time_model_ms = self.predict(device='cpu')
        perfomance_times['cpu_time_model_ms'] = cpu_time_model_ms

        if torch.cuda.is_available():
            gpu_time_model_ms = self.predict(device='cuda')
            perfomance_times['gpu_time_model_ms'] = gpu_time_model_ms

        mlflow.log_metrics(perfomance_times)

    def predict(self, device: str) -> float:
            """
            Performs emotion classification on the input texts.

            Args:
                texts (List[str]): A list of input texts to classify.

            Returns:
                Tuple[List[str], List[float]]: A tuple containing a list of predicted
                emotion classes corresponding to the input texts and
                a list of corresponding probabilities.
            """
            input_ids_list = []
            attention_mask_list = []

            for text in self.texts:
                encoding = self.tokenizer.encode_plus(
                    text,
                    add_special_tokens=True,
                    max_length=self.cfg.DATAMODULE.MAX_LENGTH,
                    padding='max_length',
                    truncation=True,
                    return_tensors='pt'
                )

                input_ids = encoding['input_ids']
                attention_mask = encoding['attention_mask']

                input_ids_list.append(input_ids)
                attention_mask_list.append(attention_mask)

            input_ids_batch = torch.cat(input_ids_list, dim=0).to(device)
            attention_mask_batch = torch.cat(attention_mask_list, dim=0).to(device)

            self.model.to(device)
            self.model.eval()
            with torch.inference_mode():
                t = time.time()
                _ = self.model(input_ids_batch, attention_mask_batch)
                ms_per_text = (time.time() - t) / len(self.texts) * 1000
            return ms_per_text

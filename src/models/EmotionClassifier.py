import os
from typing import List, Optional

import numpy as np
import pandas as pd
import pytorch_lightning as pl
import torch
from sklearn.metrics import accuracy_score, classification_report, f1_score
from torch import nn
from transformers import AutoModelForSequenceClassification


class EmotionClassifier(pl.LightningModule):
    """
    EmotionClassifier is a PyTorch Lightning module for emotion classification using
    a transformer-based model.

    Args:
        model (str): Pre-trained transformer model.
        num_classes (int): Number of emotion classes for classification.
    """

    def __init__(self,
                 base_model: str,
                 eta: float = 2e-5,
                 num_classes: int = 6,
                 device: str = 'cpu',
                 classes: Optional[List[str]] = None,
                 class_weights: torch.Tensor = None,
                 cls_report_path: Optional[str] = None):
        super().__init__()
        self.model = AutoModelForSequenceClassification.from_pretrained(
            base_model,
            num_labels=num_classes
        )
        self.criterion = nn.CrossEntropyLoss(weight=class_weights)
        self.hparams.eta = eta
        self.model_device = device
        self.preds_stage = {"train": {"loss": [],
                                      "preds": [],
                                      "targets": [],
                                      "source": []},
                            "valid": {"loss": [],
                                      "preds": [],
                                      "targets": [],
                                      "source": []},
                            "test": {"loss": [],
                                     "preds": [],
                                     "targets": [],
                                     "source": []}}

        self.cls_report_path = cls_report_path
        self.classes = classes

    def forward(self, input_ids: torch.Tensor, attention_mask: torch.Tensor):
        """
        Forward pass of the emotion classifier.

        Args:
            input_ids (torch.Tensor): Input token IDs.
            attention_mask (torch.Tensor): Attention mask for input.

        Returns:
            torch.Tensor: Logits for each class.
        """
        outputs = self.model(input_ids=input_ids, attention_mask=attention_mask)
        return outputs.logits

    def shared_step(self,
                    batch=None,
                    stage: str = None) -> torch.Tensor:
        """Common step for all stages - learning,
        model validation and testing.

        Args:
            sample (required): An image tensor. None by default.
            stage (str, required): Stage, can take values from the list:
            ['train', 'valid', 'test']. None by default.

        returns:
            torch.Tensor: Returns the loss tensor after a model pass.
        """
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        labels = batch["label"]
        source = batch["source"]

        logits = self.forward(input_ids, attention_mask)
        preds = torch.argmax(logits, 1)
        loss = self.criterion(logits, labels)

        # собираем в атрибут
        self.preds_stage[stage]['loss'].append(loss.detach().cpu())
        self.preds_stage[stage]['preds'].append(preds.detach().cpu())
        self.preds_stage[stage]['targets'].append(labels.cpu())
        self.preds_stage[stage]['source'].extend(source)

        return loss

    def shared_epoch_end(self, stage: str) -> None:
        """Function for calculating losses and accuracy at the end of the epoch
        and logging these values

        Args:
            stage (str, required): Stage, can take values from the list:
            ['train', 'valid', 'test']. Default None.
        """
        loss = self.preds_stage[stage]['loss']
        loss = torch.stack(loss)
        loss = np.mean([x.item() for x in loss])

        self.preds_stage[stage]['preds'] = (torch.cat(
            self.preds_stage[stage]['preds'])).tolist()

        self.preds_stage[stage]['targets'] = (torch.cat(
            self.preds_stage[stage]['targets'])).tolist()

        if stage == 'test':
            self.preds_stage[stage]['source'] = (np.ravel(np.array(
                self.preds_stage[stage]['source']))).tolist()

        acc = accuracy_score(self.preds_stage[stage]['targets'],
                             self.preds_stage[stage]['preds'])

        f1_macro = f1_score(self.preds_stage[stage]['targets'],
                      self.preds_stage[stage]['preds'],
                      average='macro',
                      zero_division=0)

        f1_weighted = f1_score(self.preds_stage[stage]['targets'],
                      self.preds_stage[stage]['preds'],
                      average='weighted',
                      zero_division=0)

        if self.model_device == 'mps':
            loss = loss.astype(np.float32)
            acc = acc.astype(np.float32)
            f1_macro = f1_macro.astype(np.float32)
            f1_weighted = f1_weighted.astype(np.float32)

        metrics = {
            f"{stage}_loss": loss,
            f"{stage}_acc": acc,
            f"{stage}_f1_macro": f1_macro,
            f"{stage}_f1_weighted": f1_weighted
        }
        self.log_dict(metrics, prog_bar=True)

    def _shared_clear(self, stage: str) -> None:
        """Function for cleaning lists with loss and metric values at the end
        of each epoch.

        Args:
            stage (str, optional): Стадия [train, val, test]. Defaults to None.
        """
        self.preds_stage[stage]['loss'].clear()
        self.preds_stage[stage]['preds'].clear()
        self.preds_stage[stage]['targets'].clear()
        self.preds_stage[stage]['source'].clear()

    def configure_optimizers(self):
        """
        Configure optimizer and learning rate scheduler.

        Returns:
            Tuple: Optimizer(s) and learning rate scheduler(s).
        """
        optimizer = torch.optim.AdamW(
            self.parameters(),
            self.hparams.eta
        )

        lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                       step_size=1,
                                                       gamma=0.1)
        return [optimizer], [lr_scheduler]

    def training_step(self, batch, batch_idx):
        """
        Training step.

        Args:
            batch: Batch of data.
            batch_idx (int): Batch index.

        Returns:
            torch.Tensor: Loss value.
        """
        return self.shared_step(batch=batch, stage='train')

    def validation_step(self, batch, batch_idx):
        """
        Validation step.

        Args:
            batch: Batch of data.
            batch_idx (int): Batch index.

        Returns:
            torch.Tensor: Loss value.
        """
        return self.shared_step(batch=batch, stage='valid')


    def test_step(self, batch, batch_idx):
        """
        Test step.

        Args:
            batch: Batch of data.
            batch_idx (int): Batch index.

        Returns:
            torch.Tensor: Loss value.
        """
        return self.shared_step(batch=batch, stage='test')

    def on_train_epoch_end(self) -> None:
        """Function for calculating losses and metrics at the end of the epoch"""
        self.shared_epoch_end("train")
        return self._shared_clear("train")

    def on_validation_epoch_end(self) -> None:
        """Function for calculating losses and metrics at the end of the epoch"""
        self.shared_epoch_end("valid")
        return self._shared_clear("valid")

    def on_test_epoch_end(self) -> None:
        """Function for calculating losses and metrics at the end of the epoch"""
        self.shared_epoch_end("test")

        self.log_classification_report()
        self.log_predict_by_source_report()

        return self._shared_clear("test")

    def log_classification_report(self):
        report_str = classification_report(self.preds_stage['test']['targets'],
                                    self.preds_stage['test']['preds'],
                                    target_names=self.classes,
                                    zero_division=0)
        if self.cls_report_path is None:
            report_file = "classification_report.txt"
        else:
            report_file = self.cls_report_path
        with open(report_file, "w") as file:
            file.write(report_str)
        self.logger.experiment.log_artifact(run_id=self.logger.run_id,
                                            local_path=report_file)
        if self.cls_report_path is None:
            os.remove(report_file)

    def log_predict_by_source_report(self):
        data = pd.DataFrame({
            'targets': self.preds_stage['test']['targets'],
            'preds': self.preds_stage['test']['preds'],
            'source': self.preds_stage['test']['source']
        })

        df = pd.DataFrame(data)

        correct_predictions = df[df['preds'] == df['targets']]
        correct_counts = correct_predictions.groupby('source').size()

        total_counts = df['source'].value_counts()

        accuracy = correct_counts / total_counts

        result = pd.concat([accuracy, total_counts], axis=1)
        result.columns = ['accuracy', 'support']

        result.to_csv('predict_by_source.csv', index=True)
        self.logger.experiment.log_artifact(run_id=self.logger.run_id,
                                            local_path='predict_by_source.csv')
        os.remove('predict_by_source.csv')

import argparse
from typing import List

import mlflow
import pytorch_lightning as pl
import torch
from lightning.pytorch.loggers import MLFlowLogger
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from sklearn.utils.class_weight import compute_class_weight
from yacs.config import CfgNode

from src.configs.base_config import combine_config
from src.models.EmotionClassifier import EmotionClassifier
from src.models.PerformanceChecker import PerformanceChecker
from src.scripts.utils import get_datamodule, log_config


def train(cfg: CfgNode):
    """A function to train the model.

    Args:
        cfg (CfgNode): Configuration file.
    """
    # инициализируем дата-модуль, коллбэки и mlflow логгер
    train_components = init_train_components(cfg)
    classes = train_components['data_module'].le.classes_

    class_weights = None
    if cfg.LEARNING.CLASS_WEIGTHS:
        le_classes = train_components['data_module'].le.transform(classes)
        le_labels = train_components['data_module'].train_dataset.labels
        class_weights = compute_class_weight(classes=le_classes,
                                            y=le_labels,
                                            class_weight='balanced')
        class_weights= torch.tensor(class_weights, dtype=torch.float)
        if cfg.LEARNING.DEVICE == 'gpu':
            class_weights = class_weights.to('cuda')
        else:
            class_weights = class_weights.to(cfg.LEARNING.DEVICE)

    model = EmotionClassifier(base_model=cfg.LEARNING.BASE_MODEL,
                              eta=cfg.LEARNING.ETA,
                              num_classes=cfg.LEARNING.NUM_CLASSES,
                              device=cfg.LEARNING.DEVICE,
                              classes=classes,
                              class_weights=class_weights,
                              cls_report_path=cfg.LOGGING.SAVING_CLS_REPORT_PATH)

    trainer = Trainer(
        max_epochs=cfg.LEARNING.MAX_EPOCHS,
        accelerator=cfg.LEARNING.DEVICE,
        callbacks=train_components['callbacks'],
        logger=train_components['mlf_logger']
    )

    trainer.fit(model,
                train_components['data_module'],
                ckpt_path=cfg.LEARNING.CHECKPOINT_PATH)
    trainer.test(model, datamodule=train_components['data_module'])

    torch.save(model, cfg.LOGGING.SAVING_MODEL_PATH)
    torch.save(train_components['data_module'].le, cfg.LOGGING.SAVING_LE_PATH)

    performance_checker = PerformanceChecker(
        model=model,
        cfg=cfg,
        path_to_data=cfg.DATASET_EMO.PERFORMANCE_DATA_PATH
    )
    performance_checker.start_check()

    if cfg.MLFLOW.LOG_MODEL:
        mlflow.pytorch.log_model(model, "model")
        mlflow.log_artifact(cfg.LOGGING.SAVING_LE_PATH)


def init_train_components(cfg: CfgNode):
    pl.seed_everything(cfg.LEARNING.SEED)
    experiment = mlflow.set_experiment(cfg.MLFLOW.EXPERIMENT)
    mlflow_run_id = mlflow.start_run(experiment_id=experiment.experiment_id,
                                  run_name=cfg.MLFLOW.RUN_NAME).info.run_id
    log_config(cfg)

    data_module = get_datamodule(cfg=cfg)

    callbacks = get_callbacks(cfg)
    mlf_logger = MLFlowLogger(experiment_name=cfg.MLFLOW.EXPERIMENT,
                              log_model=cfg.MLFLOW.LOG_CHECKPOINTS,
                              run_id=mlflow_run_id)

    train_components = {
        'data_module': data_module,
        'callbacks': callbacks,
        'mlf_logger': mlf_logger
    }
    return train_components


def get_callbacks(cfg: CfgNode) -> List[object]:
    """Returns a list of callbacks for training the model.
    Currently using early stops, learning rate tracking,
    model checkpoints.

    Args:
        cfg (CfgNode): Configuration object.

    returns:
        List[object]: List of callbacks.
    """

    # отслеживание lr, ранние остановки, сохранения весов
    callbacks = [
        ModelCheckpoint(
            dirpath=cfg.CHECKPOINT.CKPT_PATH,
            filename=cfg.CHECKPOINT.FILENAME,
            save_top_k=cfg.CHECKPOINT.SAVE_TOP_K,
            monitor=cfg.CHECKPOINT.CKPT_MONITOR,
            mode=cfg.CHECKPOINT.CKPT_MODE
        ),
        LearningRateMonitor(
            logging_interval=cfg.LOGGING.LOGGING_INTERVAL
        ),
        EarlyStopping(
            monitor=cfg.ES.MONITOR,
            min_delta=cfg.ES.MIN_DELTA,
            patience=cfg.ES.PATIENCE,
            verbose=cfg.ES.VERBOSE,
            mode=cfg.ES.MODE
        )
    ]

    return callbacks


def get_args_parser() -> argparse.Namespace:
    """Function for parsing the arguments specified when calling the script

    returns:
        argparse.Namespace: An object that contains arguments as attributes
    """
    parser = argparse.ArgumentParser(description="Training")
    parser.add_argument("-config_path",
                        default='src/configs/default_config.yaml',
                        type=str,
                        help="Path to the configuration file")
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args_parser()
    cfg_path = args.config_path
    cfg = combine_config(cfg_path)

    train(cfg)


import os
from typing import Optional

import mlflow
from yacs.config import CfgNode

from src.data.EmotionDataModule import EmotionDataModule


def get_datamodule(cfg: CfgNode, stage: Optional[str] = None) -> EmotionDataModule:

    dm: EmotionDataModule = EmotionDataModule(
        train_path=cfg.DATAMODULE.TRAIN_PATH,
        val_path=cfg.DATAMODULE.VAL_PATH,
        test_path=cfg.DATAMODULE.TEST_PATH,
        batch_size=cfg.DATAMODULE.BATCH_SIZE,
        max_length=cfg.DATAMODULE.MAX_LENGTH,
        num_workers=cfg.DATAMODULE.NUM_WORKERS,
        base_model=cfg.LEARNING.BASE_MODEL
        )

    dm.setup(stage=stage)

    return dm


def log_config(cfg: CfgNode):
    params = {
        'DATASET.RAW_IMG_DIR': os.path.basename(cfg.DATASET_EMO.RAW_PATH),
        'DATASET.VAL_SIZE': cfg.DATASET_EMO.VAL_SIZE,
        'DATASET.TEST_SIZE': cfg.DATASET_EMO.TEST_SIZE,

        'DATAMODULE.BATCH_SIZE': cfg.DATAMODULE.BATCH_SIZE,
        'DATAMODULE.NUM_WORKERS': cfg.DATAMODULE.NUM_WORKERS,
        'DATAMODULE.MAX_LENGTH': cfg.DATAMODULE.MAX_LENGTH,

        'LEARNING.BASE_MODEL' : cfg.LEARNING.BASE_MODEL,
        'LEARNING.ETA': cfg.LEARNING.ETA,
        'LEARNING.MAX_EPOCHS': cfg.LEARNING.MAX_EPOCHS,
        'LEARNING.DEVICE': cfg.LEARNING.DEVICE,
        'LEARNING.SEED': cfg.LEARNING.SEED
    }
    mlflow.log_params(params)
    mlflow.log_artifact(cfg.CFG_INFO.CFG_PATH)

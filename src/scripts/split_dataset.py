import argparse
from pathlib import Path

import pandas as pd

from src.configs.base_config import combine_config
from src.data.DatasetSplitter import DatasetSplitter


def split_dataset(input_path: str,
               output_path: str,
               test_size: float,
               val_size: float,
               balance_classes: bool):
    """
    Split the dataset into train, validation, and test sets.

    Parameters:
        input_path (str): The path to the input dataset.
        output_path (str): The path to save the split datasets.
        test_size (float): The proportion of data to be included in the test set.
        val_size (float): The proportion of data to be included in the validation set.
        balance_classes (bool): Whether to balance the classes in the dataset.
    """

    df = pd.read_csv(input_path)

    splitter = DatasetSplitter(target_col='labels',
                               source_col='source',
                               test_size=test_size,
                               val_size=val_size)

    train_df, val_df, test_df = splitter.split_dataset_by_emotion_and_source(df)


    output_path = Path(output_path)
    if not output_path.exists():
        output_path.mkdir(parents=True)

    train_df.to_csv(f'{output_path}/train.csv', index=False)
    val_df.to_csv(f'{output_path}/val.csv', index=False)
    test_df.to_csv(f'{output_path}/test.csv', index=False)


def get_args_parser() -> argparse.Namespace:
    """Function for parsing the arguments specified when calling the script

    returns:
        argparse.Namespace: An object that contains arguments as attributes
    """
    parser = argparse.ArgumentParser(description="Dataset Splitter")
    parser.add_argument("-config_path",
                        default='src/configs/params.yaml',
                        type=str,
                        help="Путь к конфигурационному файлу")
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args_parser()
    cfg_path = args.config_path
    cfg = combine_config(cfg_path)
    split_dataset(input_path=cfg.DATASET_EMO.RAW_PATH,
                output_path=cfg.DATASET_EMO.SPLIT_OUTPUT_PATH,
                test_size=cfg.DATASET_EMO.TEST_SIZE,
                val_size=cfg.DATASET_EMO.VAL_SIZE,
                balance_classes=cfg.DATASET_EMO.BALANCE_CLASSES)

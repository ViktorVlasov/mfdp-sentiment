# Название проекта
Проект по распознаванию эмоций в тексте, выполнен в рамках [My First Data Project 2](https://ods.ai/tracks/mfdp2) для поступления в ИТМО.

## Описание проекта

Задача анализа эмоционального окраса диалогов с оператором тех.поддержки. В качестве решения предлагается функция как сервис, т.е

- мы дообучаем ML-модели на данных заказчика, если они есть,
- развертываем модель в облаке
- предоставляем доступ к API модели

Проект разделен на два репозитория. 
В данном репозитории реализована часть, связанная с обучением моделей. 
В репозитории [mfdp-sentiment-api](https://gitlab.com/ViktorVlasov/mfdp-sentiment-api) реализован инференс модели в докере на FastApi.

### Логирование в MLFlow

Все артефакты, возникающие при обучении моделей (чекпоинты модели, метрики, гиперпараметры) логируются с использованием MLflow.
Для развертывания MLflow в проекте используется [четвертый сценарий](https://mlflow.org/docs/latest/tracking.html#scenario-4-mlflow-with-remote-tracking-server-backend-and-artifact-stores). MLflow Tracking Server развертывается на Yandex Cloud, а для хранения артефактов используется хранилище S3. Для хранения сущностей (метрик) используется база данных PostgreSQL. Обратите внимание, что MLflow не включает модуль авторизации, поэтому в проекте была реализована авторизация через HTTP Basic authentication.

### Учетные данные для авторизации, шифрование git-crypt

Переменные окружения с учетными данными для авторизации в MLflow хранятся в файле `.env`. Для версионирования и хранения данных в проекте используется DVC S3 с s3 бакетом на VK Cloud.  Учетные данные для доступа к хранилищу S3 через DVC находятся в .dvc/config. 
Файлы с учетными данными (.env и .dvc/config) зашифрованы с использованием git-crypt.

### Обучение на GPU

Модели обучались на GPU, поэтому в качестве базового образа для Docker используется образ [11.7.0-cudnn8-devel-ubuntu20.04](https://hub.docker.com/layers/nvidia/cuda/11.7.0-cudnn8-devel-ubuntu20.04/images/sha256-94ea7f5d50a76b3db32f94c6f115b8664218e3702507f49d4804235164fe319f?context=explore). Для использования gpu в докер-контейнере требуется установка [nvidia-container-toolkit](https://github.com/NVIDIA/nvidia-docker).

### Конфиг на основе YACS
Все настраиваемые параметры для обучения модели находятся в src/configs/params.yaml. Для упрощения работы с конфигурацией используется [YACS](https://github.com/rbgirshick/yacs).

Более подробно про обучение модели [здесь](https://gitlab.com).

### Датасет
Кроме того, в этом репозитории собран и подготовлен датасет, на котором проводились эксперименты. Итоговый датасет является объединением нескольких наборов данных: "cedr", "ok_ml_cup", "dairai_emotions" и "emotions_go". Некоторые из них - "dairai_emotions" и "emotions_go" - изначально были на английском языке и были переведены на русский с помощью API Yandex Translate.

## Минимальные требования

- Docker CE>=19.03 
- git-crypt
- nvidia-container-toolkit и CUDA Toolkit 11.7

*Примечание: nvidia-container-toolkit и CUDA Toolkit 11.7 необязательны для запуска контейнера, но необходимы для обучения на GPU*

## Шаги по установке проекта

1. Склонируйте репозиторий:
```bash
git clone https://gitlab.com/ViktorVlasov/mfdp-sentiment
```
2. Установите [git-crypt](https://github.com/AGWA/git-crypt). В зависимости от вашей операционной системы, выполните одну из следующих команд:
   - Для macOS: `brew install git-crypt`
   - Для Ubuntu: `sudo apt install git-crypt`

3. Получите ссылку на файл `git-crypt-key` по запросу (напишите мне в [телеграм](https://t.me/rhythm_00)). Если вы являетесь одним из менторов MFDP 2, то найдите мой ответ на последний модуль "Подготовка питчинга". В нем я прикрепил ссылку на файл. Установите переменную окружения `GIT_CRYPT_KEY_URL` со значением ссылки на файл:
```bash
export GIT_CRYPT_KEY_URL="ссылка на файл git-crypt-key"
```
## Обучение модели

1. Для проведения эксперимента вам нужно: 
- Изменить значения параметров в src/configs/params.yaml на нужные вам.
- Выполнить:
 ```bash
chmod +x docker_start.sh
./docker_start.sh
```

***Дополнительно:*** если вы проводите несколько экспериментов с одной моделью, вы можете использовать кеширование моделей, используя параметр -use_cache: 
```bash
./docker_start.sh -use_cache
```
Таким образом модели будут сохраняться у вас локально в папке проекта mfdp_sentiment/cache_models/, в ходе эксперимента папка будет монтироваться в докер-контейнер.

### Дообученые модели
На данный момент корректно работает дообучение Bert моделей из huggingface.
Модели, которые были дообучены:

| Группа моделей     | Название моделей                                                                                                                                                                                 |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Small Berts        | cointegrated/rubert-tiny, cointegrated/rubert-tiny2                                                                                                                                              | 
| Multilingual Berts | 'cointegrated/LaBSE-en-ru', 'sentence-transformers/LaBSE', 'bert-base-multilingual-cased'                                                                                                        |
| DeepPavlov Berts   | 'DeepPavlov/rubert-base-cased-sentence', 'DeepPavlov/rubert-base-cased-conversational', 'DeepPavlov/distilrubert-tiny-cased-conversational', 'DeepPavlov/distilrubert-base-cased-conversational' |
| Sber Berts         | 'sberbank-ai/sbert_large_nlu_ru', 'sberbank-ai/sbert_large_mt_nlu_ru'|                                                                                                                                                                                             |     |     |



## Организация проекта
------------

    ├── LICENSE
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── interim        <- Intermediate data that has been transformed.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── notebooks          <- Jupyter notebooks.
    │
    ├── pyproject.toml     <- The specified file format of PEP 518 which contains 
    │                         the build system requirements of Python projects.
    ├── poetry.lock        <- The poetry.lock file prevents you from automatically 
    │                         getting the latest versions of your dependencies.
    │
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── configs        <- Yacs base config script and yaml config.
    │   │   └── base_config.py
    │   │   └── params.yaml
    │   │
    │   ├── data           <- Classes responsible for working with data
    │   │   └── DatasetSplitter.py
    │   │   └── EmotionDataModule.py
    │   │   └── EmotionDataset.py
    │   │
    │   ├── models         <- Classes that implement models
    │   │   ├── EmotionClassifier.py
    │   │
    │   ├── scripts        <- Scripts that implement split dataset, training model, test mode, etc.
    │   │   └── split_dataset.py
    │   │   └── train_model.py
    │   │   └── utils.py
    │   
    ├── dvc.lock           
    ├── dvc.yaml
    ├── .dvc    
    │
    ├── Dockerfile         
    ├── .dockerignore      
    ├── docker_start.sh    
    │     
    ├── .gitattributes     
    ├── .gitignore         
    │
    ├── .env
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

#!/bin/bash

# Получаем абсолютный путь к скрипту
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Проверка наличия файла git-crypt-key и дешифрование credentials
GIT_CRYPT_KEY_FILE="$SCRIPT_DIR/git-crypt-key"
if [ ! -f $GIT_CRYPT_KEY_FILE ]; then
  # Файл git-crypt-key отсутствует, скачиваем его
  if [ -z "$GIT_CRYPT_KEY_URL" ]; then
    echo "Error: GIT_CRYPT_KEY_URL variable is not set."
    exit 1
  fi
  echo "Downloading git-crypt-key file..."
  wget --no-check-certificate "$GIT_CRYPT_KEY_URL" -O "$GIT_CRYPT_KEY_FILE"
  echo "git-crypt-key file downloaded."
  git-crypt unlock $GIT_CRYPT_KEY_FILE
fi

# Формируем абсолютный путь к локальному файлу конфигурации
LOCAL_CFG_DIR="$SCRIPT_DIR/src/configs/params.yaml"
DOCKER_CFG_DIR="/app/src/configs/params.yaml"
if [ ! -f "$LOCAL_CFG_DIR" ]; then
  echo "Warning: File $LOCAL_CFG_DIR not found."
fi

# Проверка наличия аргумента use_cache
USE_CACHE=false
if [ "$1" == "-use_cache" ]; then
  USE_CACHE=true
fi

# Создаем папку cache_models, если флаг использования кэша установлен
if [ "$USE_CACHE" = true ]; then
  LOCAL_CACHE_DIR="$SCRIPT_DIR/cache_models"
  DOCKER_CACHE_DIR="/app/cache_models"
  mkdir -p "$LOCAL_CACHE_DIR"
fi

# Путь к папке с логами, создаем папку, если её ещё нет
LOCAL_LOGS_DIR="$SCRIPT_DIR/logs"
DOCKER_LOGS_DIR="/app/logs"
mkdir -p "$LOCAL_LOGS_DIR"

IMAGE_NAME="docker_mfdp"
# Проверка наличия образа, если отсутствует, то выполняем сборку
if [[ "$(docker images -q $IMAGE_NAME 2>/dev/null)" == "" ]]; then
  echo "Build docker image..."
  docker build -t $IMAGE_NAME .
  echo "Docker image built."
fi

#Путь к dvc.lock
LOCAL_DVC_LOCK="$SCRIPT_DIR/dvc.lock"
DOCKER_DVC_LOCK="/app/dvc.lock"

LOCAL_SRC="$SCRIPT_DIR/src"
DOCKER_SRC="/app/src"

docker run --rm --gpus all --env-file .env\
    -v $LOCAL_CFG_DIR:$DOCKER_CFG_DIR \
    -v $LOCAL_LOGS_DIR:$DOCKER_LOGS_DIR \
    -v $LOCAL_CACHE_DIR:$DOCKER_CACHE_DIR \
    -v $LOCAL_DVC_LOCK:$DOCKER_DVC_LOCK \
    -v $LOCAL_SRC:$DOCKER_SRC \
    $IMAGE_NAME dvc repro


FROM nvidia/cuda:11.7.1-cudnn8-devel-ubuntu20.04

WORKDIR /app

COPY . .

# устанавливаем python 3.10 и pip
RUN apt-get update && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y software-properties-common && \
    DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get install -y python3.10 curl && \
    curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10 && \
    ln -s /usr/bin/python3.10 /usr/bin/python

# Скачиваем и устанавливаем poetry
RUN pip install poetry==1.5.1 && \
    pip uninstall -y distro-info && pip install distro-info==1.0 && \
    poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi

ENV GIT_PYTHON_REFRESH=quiet
# скачиваем данные
RUN dvc pull data/raw/six_emotions.csv.dvc && \
    dvc pull data/raw/mini_six_emotions.csv.dvc && \
    dvc pull data/raw/performance_test.csv.dvc

# Путь к конфигу и кеширование моделей
ENV YACS_CFG_PATH=src/configs/params.yaml
ENV HF_HOME=/app/cache_models

CMD ["bash"]